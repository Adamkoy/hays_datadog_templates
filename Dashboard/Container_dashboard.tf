resource "datadog_dashboard" "container_ordered_dashboard" {
  title         = "Ireland DevOps Container Dashboard A"
  description   = "Created using the Datadog provider in Terraform"
  layout_type   = "ordered"
  is_read_only  = false


widget {
  timeseries_definition {
    request {
      q= "sum:docker.containers.running{$environment,$docker_image,$owner,$host_name,$instance-type,$AZ,$host} by {availability-zone,instance-type, host}"
      display_type = "bars"
      style {
        palette = "classic"

      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }
      title = "Running Docker Containers"
      show_legend = true
      time = {
        live_span = "4h"
      }

      yaxis {
        scale = "linear"
        include_zero = true
        max = true
  }
}
}

widget {
  timeseries_definition {
    request {
      q= "sum:docker.containers.running{$environment,$docker_image,$owner,$host_name,$instance-type,$AZ,$host} by {availability-zone,instance-type, host, docker_image}"
      display_type = "bars"
      style {
        palette = "classic"

      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }
      title = "Running Containers With Image Name "
      show_legend = true
      time = {
        live_span = "4h"
      }

      yaxis {
        scale = "linear"
        include_zero = true
        max = true
  }
}
}


  widget {
  toplist_definition {
    request {
      q= "sum:docker.containers.running{$environment,$owner,$host_name,$instance-type,$AZ,$host} by {availability-zone,docker_image,instance-type, host}"

    }
    title = "Total Running Containers"
  }
}


widget {
toplist_definition {
  request {
    q= "avg:docker.cpu.user{$environment,$owner,$host_name,$instance-type,$AZ,$host} by {host, instance,availability-zone}"

  }
  title = "Most CPU-intensive containers"
}
}


widget {
  timeseries_definition {
    request {
      q= "avg:docker.cpu.user{$environment,$owner,$host_name,$instance-type,$AZ,$host} by {availability-zone,docker_image,instance-type, host}"
      display_type = "line"
      style {
        palette = "classic"
        line_type = "solid"
        line_width = "thin"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }
    request {
      log_query {
        index = "mcnulty"
        compute = {
          aggregation = "avg"
          facet = "@duration"
          interval = 5000
        }
        search = {
          query = "status:info"
        }
        group_by {
          facet = "host"
          limit = 10
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
          }
        }
      }
      display_type = "area"
    }
    title = "CPU user by image"
    show_legend = true
    time = {
      live_span = "1h"
    }

    yaxis {
      scale = "linear"
      include_zero = true
      max = true
    }
  }
}


widget {
  timeseries_definition {
    request {
      q= "avg:docker.cpu.system{$environment,$owner,$host_name,$instance-type,$AZ,$host} by {availability-zone,docker_image,instance-type, host}"
      display_type = "line"
      style {
        palette = "classic"
        line_type = "solid"
        line_width = "thin"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }
    request {
      log_query {
        index = "mcnulty"
        compute = {
          aggregation = "avg"
          facet = "@duration"
          interval = 5000
        }
        search = {
          query = "status:info"
        }
        group_by {
          facet = "host"
          limit = 10
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
          }
        }
      }
      display_type = "area"
    }
    title = "CPU system by image"
    show_legend = true
    time = {
      live_span = "1h"
    }

    yaxis {
      scale = "linear"
      include_zero = true
      max = true
    }
  }
}

widget {
toplist_definition {
  request {
    q= "avg:docker.mem.rss{$environment,$owner,$host_name,$instance-type,$AZ,$host} by {availability-zone,container_name,host}"

  }
  title = "Most RAM-intensive containers"
}
}

widget {
  heatmap_definition {
    request {
      q = "avg:docker.cpu.user{$environment,$owner,$host_name,$instance-type,$AZ,$host} by {availability-zone,instance-type, host}"
      style {
        palette = "orange"
      }
    }
    yaxis {
      min = true
      max = true
      include_zero = false
      scale = "sqrt"
    }
    title = "CPU by container "
    time = {
      live_span = "1h"
    }
  }
}


    widget {
      hostmap_definition {
        request {
          fill {
            q = "sum:kubernetes.cpu.usage.total{$environment,$AZ,$instance-type,$host_name} by {*} "
          }
        }
        node_type= "host"
        no_group_hosts = true
        no_metric_hosts = false
        style {
          palette = "yellow_to_green"
          palette_flip = false
          fill_min = true
          fill_max = true
        }
        title = "CPU Utilization per Kubernetes node"
      }
     }

     template_variable {
       name   = "environment"
       prefix = "environment"
       default = "*"
     }

template_variable {
  name   = "host"
  prefix = "host"
  default = "*"
}

template_variable {
  name   = "AZ"
  prefix = "availability-zone"
  default = "*"
}
template_variable {
  name   = "instance-type"
  prefix = "instance-type"
  default = "*"
}

template_variable {
  name   = "AWS_account"
  prefix = "aws_account"
  default = "*"
}
template_variable {
   name   = "host_name"
   prefix = "instance_id"
   default = "*"
 }


 template_variable {
      name   = "owner"
      prefix = "owner"
      default = "*"
 }

 template_variable {
      name   = "docker_image"
      prefix = "docker_image"
      default = "*"
 }


}
