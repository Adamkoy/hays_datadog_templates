resource "datadog_dashboard" "EBS_dashboard" {
  title         = "Ireland DevOps AWS EBS A"
  description   = "Created using the Datadog provider in Terraform"
  layout_type   = "ordered"
  is_read_only  = true


########## SEC PER OP READ TIME   1  ###################

  widget {
    timeseries_definition {
      request {
        q= "aws.ebs.volume_read_ops{$AZ,$instance-type,$host_name,$host_id,$environment} by {host,instance-type}"
        display_type = "line"
        style {
          palette = "classic"
          line_type = "solid"
          line_width = "normal"
        }
        metadata {
          expression = "instance-type"
          alias_name = "Alpha"
        }
      }
      request {
        log_query {
          index = "mcnulty"
          compute = {
            aggregation = "avg"
            facet = "@duration"
            interval = 5000
          }
          search = {
            query = "status:info"
          }
          group_by {
            facet = "host"
            limit = 50
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
              interval = 5000
            }
          }
          group_by {
            facet = "instance-type"
            limit = 50
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
              interval = 5000
            }
          }
        }
        display_type = "line"
      }
      title = "Read time, by host/instance type (sec per op)"
      show_legend = true
      time = {
        live_span = "1h"
      }
      yaxis {
        scale = "linear"
        include_zero = true
        max = true
      }
    }
  }

############################## EBS VOLUME TOTAL WRITE PER MS 2  ################
  widget {
    timeseries_definition {
      request {
        q= "aws.ebs.volume_total_write_time{$AZ,$instance-type,$host_name,$host_id,$environment} by {host,instance-type}"
        display_type = "area"
        style {
          palette = "classic"
          line_type = "solid"
          line_width = "normal"
        }
        metadata {
          expression = "instance-type"
          alias_name = "Alpha"
        }
      }
      request {
        log_query {
          index = "mcnulty"
          compute = {
            aggregation = "avg"
            facet = "@duration"
            interval = 5000
          }
          search = {
            query = "status:info"
          }
          group_by {
            facet = "host"
            limit = 50
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
              interval = 1000
            }
          }
          group_by {
            facet = "instance_type"
            limit = 50
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
              interval = 1000
            }
          }
        }
        display_type = "area"
      }
      title = "Total write  host/instance-type (sec per op)"
      show_legend = true
      time = {
        live_span = "1h"
      }
      yaxis {
        scale = "linear"
        include_zero = true
        max = true
      }
    }
  }
  #### READ OPERATION PER minute  3 ###########################

  widget {
    timeseries_definition {
      request {
        q= "aws.ebs.volume_write_ops{$AZ,$instance-type,$host_name,$host_id,$environment} by {host,instance-type}"
        display_type = "area"
        style {
          palette = "classic"
          line_type = "solid"
          line_width = "normal"
        }
        metadata {
          expression = "instance-type"
          alias_name = "Alpha"
        }
      }
      request {
        log_query {
          index = "mcnulty"
          compute = {
            aggregation = "avg"
            facet = "@duration"
            interval = 5000
          }
          search = {
            query = "status:info"
          }
          group_by {
            facet = "host"
            limit = 50
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
              interval = 1000
            }
          }
          group_by {
            facet = "instance-type"
            limit = 50
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
              interval = 1000
            }
          }
        }
        display_type = "area"
      }
      title = "read ops host/instance-type (ops per minute)"
      show_legend = true
      time = {
        live_span = "1h"
      }
      yaxis {
        scale = "linear"
        include_zero = true
        max = true
      }
    }

  }
#### READ OPERATION PER minute  4 ###########################

widget {
  timeseries_definition {
    request {
      q= "aws.ebs.volume_write_ops{$AZ,$instance-type,$host_name,$host_id,$environment} by {host,instance-type}"
      display_type = "line"
      style {
        palette = "classic"
        line_type = "solid"
        line_width = "normal"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }
    request {
      log_query {
        index = "mcnulty"
        compute = {
          aggregation = "avg"
          facet = "@duration"
          interval = 5000
        }
        search = {
          query = "status:info"
        }
        group_by {
          facet = "host"
          limit = 50
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
            interval = 1000
          }
        }
        group_by {
          facet = "instance-type"
          limit = 50
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
            interval = 1000
          }
        }
      }
      display_type = "line"
    }
    title = "write ops host/instance-type (ops per minute)"
    show_legend = true
    time = {
      live_span = "1h"
    }
    yaxis {
      scale = "linear"
      include_zero = true
      max = true
    }
  }

}



#### Disk bytes read per sec 5 ###########################

widget {
  timeseries_definition {
    request {
      q= "aws.ebs.volume_read_bytes{$AZ,$instance-type,$host_name,$host_id,$environment} by {host,instance-type}"
      display_type = "area"
      style {
        palette = "classic"
        line_type = "solid"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }
    request {
      log_query {
        index = "mcnulty"
        compute = {
          aggregation = "avg"
          facet = "@duration"
          interval = 5000
        }
        search = {
          query = "status:info"
        }
        group_by {
          facet = "host"
          limit = 50
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
            interval = 1000
          }
        }
        group_by {
          facet = "instance-type"
          limit = 50
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
            interval = 1000
          }
        }
      }
      display_type = "area"
    }
    title = "Disk read bytes host/instance-type (bytes per sec)"
    show_legend = true
    time = {
      live_span = "1h"
    }
    yaxis {
      scale = "linear"
      include_zero = true
      max = true
    }
  }

}
################ Disk bytes written, by host/instance type (bytes per sec) 6   ############################
widget {
  timeseries_definition {
    request {
      q= "aws.ebs.volume_write_bytes.sum{$AZ,$instance-type,$host_name,$host_id,$environment} by {host,instance-type}"
      display_type = "area"
      style {
        palette = "classic"
        line_type = "solid"

      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }
    request {
      log_query {
        index = "mcnulty"
        compute = {
          aggregation = "avg"
          facet = "@duration"
          interval = 5000
        }
        search = {
          query = "status:info"
        }
        group_by {
          facet = "host"
          limit = 50
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
            interval = 1000
          }
        }
        group_by {
          facet = "instance-type"
          limit = 50
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
            interval = 1000
          }
        }
      }
      display_type = "area"
    }
    title = "Sum of bytes written in Disk, host/instance-type (bytes per sec)"
    show_legend = true
    time = {
      live_span = "1h"
    }
    yaxis {
      scale = "linear"
      include_zero = true
      max = true
    }
  }

}
##################### Queue length, by device (ops)   ####################
widget {
  timeseries_definition {
    request {
      q= "aws.ebs.volume_queue_length{$AZ,$instance-type,$host_name,$host_id,$environment} by {host,instance-type}"
      display_type = "area"
      style {
        palette = "classic"
        line_type = "solid"

      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }
    request {
      log_query {
        index = "mcnulty"
        compute = {
          aggregation = "avg"
          facet = "@duration"
          interval = 5000
        }
        search = {
          query = "status:info"
        }
        group_by {
          facet = "host"
          limit = 50
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
            interval = 1000
          }
        }
        group_by {
          facet = "instance-type"
          limit = 50
          sort = {
            aggregation = "avg"
            order = "desc"
            facet = "@duration"
            interval = 1000
          }
        }
      }
      display_type = "area"
    }
    title = "Queue length, host/instance-type"
    show_legend = true
    time = {
      live_span = "1h"
    }
    yaxis {
      scale = "linear"
      include_zero = true
      max = true
    }
  }

}

template_variable {
  name   = "environment"
  prefix = "environment"
  default = "*"
}

template_variable {
  name   = "host_id"
  prefix = "host"
  default = "*"
}

  template_variable {
    name   = "AZ"
    prefix = "availability-zone"
    default = "*"
  }

  template_variable {
    name   = "instance-type"
    prefix = "instance-type"
    default = "*"
  }
  template_variable {
    name   = "host_name"
    prefix = "instance_id"
    default = "*"
  }
  #Container name
}
