resource "datadog_dashboard" "ordered_dashboard" {
  title         = "Ireland DevOps EC2 Instances A"
  description   = "Created using the Datadog provider in Terraform"
  layout_type   = "ordered"
  is_read_only  = true


  widget {
    timeseries_definition {
      request {
        q= "aws.ec2.cpuutilization{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {instance-type, host,availability-zone}"
        display_type = "line"
        style {
          palette = "classic"
          line_type = "solid"
          line_width = "thin"
        }
        metadata {
          expression = "instance-type"
          alias_name = "Alpha"
        }
      }
      request {
        log_query {
          index = "mcnulty"
          compute = {
            aggregation = "avg"
            facet = "@duration"
            interval = 5000
          }
          search = {
            query = "status:info"
          }
          group_by {
            facet = "host"
            limit = 10
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
            }
          }
        }
        display_type = "area"
      }
      title = "AWS EC2 CPU Utilization"
      show_legend = true
      time = {
        live_span = "1h"
      }
      event {
        q = "sources:test tags:1"
      }
      event {
        q = "sources:test tags:2"
      }
      yaxis {
        scale = "linear"
        include_zero = true
        max = 100
      }
    }
  }


  widget {
    toplist_definition {
      request {
        q= "avg:${var.cpu_utilization}{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by ${var.grouped_by}"
        conditional_formats {
          comparator = "<"
          value = "2"
          palette = "white_on_green"
        }
        conditional_formats {
          comparator = ">"
          value = "2.2"
          palette = "white_on_red"
        }
      }
      title = "AWS EC2 CPU Utilization"
    }
  }


  widget {
    heatmap_definition {
      request {
        q = "avg:aws.ec2.cpuutilization{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {instance-type, host}"
        style {
          palette = "orange"
        }
      }
      yaxis {
        min = 0
        max = 99
        include_zero = true
        scale = "sqrt"
      }
      title = "Heatmap For CPU Utilization "
      time = {
        live_span = "1h"
      }
    }
  }






  widget {
    toplist_definition {
      request {
        q= "aws.ec2.network_in{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {instance-type,host}"

      }
      title = "Network In By Instance Type"
    }
  }





  widget {
    change_definition {
      request {
        q = "aws.ec2.cpuutilization{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {instance-type}"
        change_type = "absolute"
        compare_to = "week_before"
        increase_good = true
        order_by = "name"
        order_dir = "desc"
        show_present = true
      }
      title = "AWS EC2 CPU Utilization Comparison One Week Before"
      time = {
        live_span = "4h"
      }
    }
  }


  widget {
    query_value_definition {
      request {
        q = "aws.ec2.host_ok{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment}"
        aggregator = "sum"
      }
      autoscale = true

      precision = "2"
      text_align = "right"
      title = "EC2 Host Ok For Availability Zone"
      time = {
        live_span = "1h"
      }
    }
  }

  widget {
    hostmap_definition {
      request {
        fill {
          q = "avg:aws.ec2.cpuutilization{*} by {*}"
        }
      }
      node_type= "host"
      group = ["availability-zone", "instance-type"]
      no_group_hosts = true
      no_metric_hosts = true
      #scope = ["$type_of_EC2, $owner, $host_name, $AWS_account, $instance-type, $AZ, $host"]
      scope = ["$type_of_EC2", "$owner", "$host_name", "$AWS_account", "$instance-type", "$host"]

      style {
        palette = "yellow_to_green"
        palette_flip = true
        fill_min = "10"
        fill_max = "20"
      }
      title = "Hosts In Availability Zones"
    }
  }



  template_variable {
    name   = "environment"
    prefix = "environment"
    default = "*"
  }
   template_variable {
     name   = "host"
     prefix = "host"
     default = "*"
   }

   template_variable {
     name   = "AZ"
     prefix = "availability-zone"
     default = "*"
   }
   template_variable {
     name   = "instance-type"
     prefix = "instance-type"
     default = "*"
   }


   template_variable {
      name   = "host_name"
      prefix = "instance_id"
      default = "*"
    }

    template_variable {
         name   = "owner"
         prefix = "owner"
         default = "*"
    }
    template_variable {
         name   = "type_of_EC2"
         prefix = "type"
         default = "*"
    }

  }
