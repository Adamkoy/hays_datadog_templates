resource "datadog_dashboard" "node_ordered_dashboard" {
  title         = "Ireland DevOps Kubernetes A"
  description   = "Created using the Datadog provider in Terraform"
  layout_type   = "ordered"
  is_read_only  = false



  widget {
    timeseries_definition {
      request {
        q= "sum:kubernetes.pods.running{$docker_image,$host,$AZ,$instance-type,$host_name, $kube_deployment,$environment} by {availability-zone,instance-type,host}"
        display_type = "bars"
        style {
          palette = "classic"
          line_type = "solid"
          line_width = "thin"
        }
        metadata {
          expression = "instance-type"
          alias_name = "Alpha"
        }
      }
        title = "Running Kubernetes Pods"
        show_legend = true
        time = {
          live_span = "1h"
        }

        yaxis {
          scale = "linear"
          include_zero = true
          max = true
    }
  }
}

    widget {
      timeseries_definition {
        request {
          q= "sum:kubernetes_state.deployment.replicas_unavailable{$host,$kube_namespace,$kube_deployment,$AZ,$instance-type,$host_name,$pod_name,$owner,$docker_image,$environment} by {availability-zone,deployment,instance-type,host}"
          display_type = "bars"
          style {
            palette = "classic"
            line_type = "solid"
            line_width = "thin"
          }
          metadata {
            expression = "instance-type"
            alias_name = "Alpha"
          }
        }
          title = "Pods Unavailable"
          show_legend = true
          time = {
            live_span = "4h"
          }

          yaxis {
            scale = "linear"
            include_zero = true
            max = true
      }
    }
    }

  widget {
    timeseries_definition {
      request {
        q= "sum:kubernetes_state.pod.ready{$docker_image,$host,$kube_namespace,$kube_deployment,$AZ,$instance-type,$host_name,$pod_name,$environment,$owner} by {availability-zone,host, instance-type} "
        display_type = "bars"
        style {
          palette = "classic"
          line_type = "solid"
          line_width = "thin"
        }
        metadata {
          expression = "instance-type"
          alias_name = "Alpha"
        }
      }
        title = "Kubernetes Pods Ready by Host/ Instance type"
        show_legend = true
        time = {
          live_span = "4h"
        }

        yaxis {
          scale = "linear"
          include_zero = true
          max = true
    }
  }
  }



    widget {
      hostmap_definition {
        request {
          fill {
            q = "sum:kubernetes.cpu.usage.total{$host,$kube_namespace,$kube_deployment,$AZ,$instance-type,$host_name,$pod_name,$environment,$owner} by {availability-zone,deployment,instance-type,host} "
          }
        }
        node_type= "host"
        no_group_hosts = true
        no_metric_hosts = false
        style {
          palette = "yellow_to_green"
          palette_flip = false
          fill_min = true
          fill_max = true
        }
        title = "CPU Utilization per node"
      }
     }

     widget {
       hostmap_definition {
         request {
           fill {
             q = "sum:kubernetes.memory.usage{$host,$kube_namespace,$kube_deployment,$AZ,$instance-type,$host_name,$pod_name,$environment,$owner} by {availability-zone,deployment,instance-type,host} "
           }
         }
         node_type= "host"
         no_group_hosts = true
         no_metric_hosts = false
         style {
           palette = "cool"
           palette_flip = false
           fill_min = true
           fill_max = true
         }
         title = "Memory usage per node"
       }
      }
  widget {
    timeseries_definition {
      request {
        q= "sum:kubernetes.cpu.requests{$host,$kube_namespace,$kube_deployment,$AZ,$instance-type,$host_name,$pod_name,$environment,$owner} by {availability-zone,deployment,instance-type,host} "
        display_type = "line"
        style {
          palette = "cool"
          line_type = "solid"
          line_width = "thin"
        }
        metadata {
          expression = "instance-type"
          alias_name = "Alpha"
        }
      }

      title = "Kubernetes CPU requests per node by host/instance type"
      show_legend = true
      time = {
        live_span = "4h"
      }

      yaxis {
        scale = "linear"
        include_zero = true
        max = true
      }
    }
  }


        widget {
          timeseries_definition {
            request {
              q= "sum:kubernetes.memory.requests{$host,$kube_namespace,$kube_deployment,$AZ,$instance-type,$host_name,$pod_name,$environment,$owner} by {availability-zone,deployment,instance-type,host} "
              display_type = "line"
              style {
                palette = "classic"
                line_type = "solid"
                line_width = "thin"
              }
              metadata {
                expression = "instance-type"
                alias_name = "Alpha"
              }
            }
              title = "Sum Kubernetes memory requests per node by host /instance type"
              show_legend = true
              time = {
                live_span = "1h"
              }

              yaxis {
                scale = "linear"
                include_zero = true
                max = true
          }
        }
      }



  widget {
    toplist_definition {
      request {
        q= "sum:kubernetes.memory.usage {$docker_image,$host,$kube_namespace,$kube_deployment,$AZ,$instance-type,$host_name,$pod_name,$environment,$owner} by {availability-zone,deployment,instance-type,host}  "

      }
      title = "Most memory-intensive pods by host/instance type"
    }
  }

    widget {
      toplist_definition {
        request {
          q= "avg:docker.cpu.user {$docker_image,$host,$kube_namespace,$kube_deployment,$AZ,$instance-type,$host_name,$pod_name,$environment,$owner} by {availability-zone,deployment,instance-type,host}  "

        }
        title = "Most CPU-intensive pods by host/ instance type"
      }
    }


    template_variable {
      name   = "environment"
      prefix = "environment"
      default = "*"
    }



 template_variable {
   name   = "host"
   prefix = "host"
   default = "*"
 }
 template_variable {
   name   = "kube_namespace"
   prefix = "service_name"
   default = "*"
 }

 template_variable {
   name   = "kube_deployment"
   prefix = "kube_deployment"
   default = "*"
 }
 template_variable {
   name   = "AZ"
   prefix = "availability-zone"
   default = "*"
 }
 template_variable {
   name   = "instance-type"
   prefix = "instance-type"
   default = "*"
 }

 template_variable {
   name   = "AWS_account"
   prefix = "aws_account"
   default = "*"
 }
 template_variable {
    name   = "host_name"
    prefix = "instance_id"
    default = "*"
  }

  template_variable {
     name   = "pod_name"
     prefix = "pod_name"
     default = "*"
  }


  template_variable {
       name   = "owner"
       prefix = "owner"
       default = "*"
  }
  template_variable {
       name   = "docker_image"
       prefix = "docker_image"
       default = "*"
  }

}
