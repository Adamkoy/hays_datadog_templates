
resource "datadog_dashboard" "RDS_dashboard" {
  title         = "Ireland DevOps RDS A"
  description   = "Created using the Datadog provider in Terraform"
  layout_type   = "ordered"
  is_read_only  = false





widget {
  timeseries_definition {
    request {
      q= "top10(max:aws.rds.database_connections{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier})"
      display_type = "line"
      style {
        palette = "classic"
        line_type = "solid"
        line_width = "thin"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }

    title = "Connections by instance, top 10 past day"
    show_legend = true
    time = {
      live_span = "1d"
    }

    yaxis {
      scale = "linear"
      include_zero = true
      max = true
    }
  }
}


widget {
  toplist_definition {
    request {
      q= "top10(max:aws.rds.database_connections{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier})"
      conditional_formats {
        comparator = "<"
        value = "2"
        palette = "white_on_green"
      }
      conditional_formats {
        comparator = ">"
        value = "2.2"
        palette = "white_on_red"
      }
    }
    title = "Connections by instance, past day"
  }
}



######################### Read operations per second by instance, top 10 past day ########################
widget {
  timeseries_definition {
    request {
      q= "top10(avg:aws.rds.read_iops{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier}.as_count())"
      display_type = "line"
      style {
        palette = "classic"
        line_type = "solid"
        line_width = "thin"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }

    title = "Read operations per second "
    show_legend = true
    time = {
      live_span = "1h"
    }

    yaxis {
      scale = "linear"
      include_zero = true
      max = true
    }
  }
}


widget {
  toplist_definition {
    request {
      q= "top10(avg:aws.rds.read_iops{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier})"
      conditional_formats {
        comparator = "<"
        value = "2"
        palette = "white_on_green"
      }
      conditional_formats {
        comparator = ">"
        value = "2.2"
        palette = "white_on_red"
      }
    }
    title = "Read operations per second by instance, top 10 past day"
    time = {
      live_span = "1h"
    }

  }
}



widget {
  timeseries_definition {
    request {
      q= "top10(avg:aws.rds.read_latency{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier}.as_count())"
      display_type = "line"
      style {
        palette = "classic"
        line_type = "solid"
        line_width = "thin"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }

    title = "Read Latency by instance, top 10 past day"

    yaxis {

      include_zero = true
      max = true

    }
  }
}


widget {
  toplist_definition {
    request {
      q= "top10(avg:aws.rds.read_latency{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier})"
      conditional_formats {
        comparator = "<"
        value = "2"
        palette = "white_on_green"
      }
      conditional_formats {
        comparator = ">"
        value = "2.2"
        palette = "white_on_red"
      }
    }
    title = "Read latency by instance, past day"
    time = {
      live_span = "1d"
    }

  }
}


widget {
  timeseries_definition {
    request {
      q= "top10(avg:aws.rds.cpuutilization{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier})"
      display_type = "line"
      style {
        palette = "classic"
        line_type = "solid"
        line_width = "thin"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }

    title = "CPU by instance (%), top 10 past day"

    yaxis {

      include_zero = true
      max = true

    }
  }
}



widget {
  toplist_definition {
    request {
      q= "top10(avg:aws.rds.cpuutilization{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier})"

    }
    title = "CPU by instance, past day"
    time = {
      live_span = "1d"
    }

  }
}



widget {
  timeseries_definition {
    request {
      q= "avg:aws.rds.freeable_memory{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier}"
      display_type = "line"
      style {
        palette = "classic"
        line_type = "solid"
        line_width = "thin"
      }
      metadata {
        expression = "instance-type"
        alias_name = "Alpha"
      }
    }

    title = "CPU by instance (%), top 10 past day"

    yaxis {

      include_zero = true
      max = true

    }
  }
}


widget {
  toplist_definition {
    request {
      q= "avg:aws.rds.freeable_memory{$type_of_EC2,$owner,$host_name,$instance-type,$AZ,$host,$environment} by {dbinstanceidentifier}"

    }
    title = "Available RAM by instance, past day"
    time = {
      live_span = "1d"
    }

  }
}


template_variable {
  name   = "environment"
  prefix = "environment"
  default = "*"
}

   template_variable {
     name   = "host"
     prefix = "host"
     default = "*"
   }

   template_variable {
     name   = "AZ"
     prefix = "availability-zone"
     default = "*"
   }
   template_variable {
     name   = "instance-type"
     prefix = "instance-type"
     default = "*"
   }

   template_variable {
     name   = "AWS_account"
     prefix = "aws_account"
     default = "*"
   }
   template_variable {
      name   = "host_name"
      prefix = "instance_id"
      default = "*"
    }

    template_variable {
         name   = "owner"
         prefix = "owner"
         default = "*"
    }
    template_variable {
         name   = "type_of_EC2"
         prefix = "type"
         default = "*"
    }

  }
