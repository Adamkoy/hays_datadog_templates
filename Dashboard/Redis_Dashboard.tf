# resource "datadog_dashboard" "REDIS_dashboard" {
#   title         = "Ireland DevOps AWS Redis Metrics"
#   description   = "Created using the Datadog provider in Terraform. This will track the metrics redis"
#   layout_type   = "ordered"
#   is_read_only  = true
#
#
#
# ################## Commands per second ########################
# widget {
#     timeseries_definition {
#       request {
#         q= "sum:redis.net.commands{$Production_env} by {region,instance-type,type,redis_host}"
#         display_type = "line"
#         style {
#           palette = "classic"
#           line_type = "solid"
#           line_width = "normal"
#         }
#         metadata {
#           expression = "instance-type"
#           alias_name = "Alpha"
#         }
#       }
#
#       title = "Commands per second"
#       show_legend = true
#       time = {
#         live_span = "1h"
#       }
#       yaxis {
#         scale = "linear"
#         include_zero = true
#         max = true
#       }
#     }
#   }
#
#
# ################## CACHE HIT RATE  ########################
#
# widget {
#     timeseries_definition {
#       request {
#         q= "avg:redis.stats.keyspace_hits{$Production_env}"
#         display_type = "line"
#         style {
#           palette = "warm"
#           line_type = "dashed"
#           line_width = "thin"
#         }
#       }
#       request {
#         metric {
#           index = "mcnulty"
#           compute = {
#             aggregation = "avg"
#             facet = "@duration"
#             interval = 5000
#           }
#           search = {
#             query = "avg:redis.stats.keyspace_misses{$Production_env}"
#           }
#           group_by {
#             facet = "host"
#             limit = 10
#             sort = {
#               aggregation = "avg"
#               order = "desc"
#               facet = "@duration"
#             }
#           }
#         }
#         display_type = "line"
#       }
#       request {
#         apm_query {
#           index = "apm-search"
#           compute = {
#             aggregation = "avg"
#             facet = "@duration"
#             interval = 5000
#           }
#           search = {
#             query = "type:web"
#           }
#           group_by {
#             facet = "resource_name"
#             limit = 50
#             sort = {
#               aggregation = "avg"
#               order = "desc"
#               facet = "@string_query.interval"
#             }
#           }
#         }
#         display_type = "line"
#       }
#       request {
#         process_query {
#           metric = "process.stat.cpu.total_pct"
#           search_by = "error"
#           filter_by = ["active"]
#           limit = 50
#         }
#         display_type = "area"
#       }
#
#       marker {
#           display_type = "ok/green"
#           label = "ideal hit rate"
#           value = "100"
#         }
#         marker {
#           display_type = "error/red"
#           label = "ideal hit rate"
#           value = "75"
#         }
#
#       title = "Cache hit rate"
#       show_legend = true
#       time = {
#         live_span = "1h"
#       }
#       event {
#         q = "sources:test tags:1"
#       }
#       event {
#         q = "sources:test tags:2"
#       }
#       yaxis {
#         scale = "log"
#         include_zero = false
#         max = true
#       }
#     }
#   }
#
#
#
#
#   template_variable {
#        name   = "Production_env"
#        prefix = "production"
#        default = "*"
#   }
# }
