resource "datadog_dashboard" "REDIS_dashboard" {
  title         = "Ireland DevOps AWS Redis Metrics"
  description   = "Created using the Datadog provider in Terraform. This will track the metrics redis"
  layout_type   = "ordered"
  is_read_only  = true

  widget {
    timeseries_definition {
      request {
        q= "sum:redis.net.commands{*}"
        display_type = "line"
        style {
          palette = "classic"
          line_type = "solid"
          line_width = "normal"
        }
        metadata {
          expression = "instance-type"
          alias_name = "Alpha"
        }
      }
      request {
        log_query {
          index = "mcnulty"
          compute = {
            aggregation = "avg"
            facet = "@duration"
            interval = 5000
          }
          search = {
            query = "status:info"
          }
          group_by {
            facet = "host"
            limit = 50
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
              interval = 5000
            }
          }
          group_by {
            facet = "instance-type"
            limit = 50
            sort = {
              aggregation = "avg"
              order = "desc"
              facet = "@duration"
              interval = 5000
            }
          }
        }
        display_type = "line"
      }
      title = "Commands per second"
      show_legend = true
      time = {
        live_span = "1h"
      }
      yaxis {
        scale = "linear"
        include_zero = true
        max = true
      }
    }
  }
}
