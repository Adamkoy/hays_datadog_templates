# Create a new Datadog monitor
# resource "datadog_monitor" "test_hys" {
#   name               = "A_test Ireland_DevOps_Test monitoring for EC2"
#   type               = "metric alert"
#   message            = "Monitor triggered. Notify: @slack"
#   escalation_message = "Escalation message @a.koyuncu@cyc.ltd"
#
#   query = "avg(last_1h):avg:aws.ec2.cpuutilization{availability-zone:eu-west-1a} by {instance-type} > 90"
#
#   thresholds = {
#     ok                = 72
#     warning           = 80
#     warning_recovery  = 79
#     critical          = 90
#     critical_recovery = 75
#   }
#
#   notify_no_data    = false
#   renotify_interval = 90
#
#   notify_audit = false
#   timeout_h    = 60
#   include_tags = true
#
#   silenced = {
#     "*" = 0
#   }
#
# }
