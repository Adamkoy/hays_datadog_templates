
resource "datadog_monitor" "Kub_memory_requests" {
  name               = "eks kubernetes memory requests A"
  type               = "metric alert"
  message            = "Monitor triggered. Notify: @slack"
  escalation_message = "Escalation message @a.koyuncu@cyc.ltd"

  query = "avg(last_1h):avg:kubernetes.memory.requests{*} by {pod_name,instance-type,kube_namespace,docker_image,image} > 600000000"

  thresholds = {
    ok                = 550000000
    warning           = 585000000
    warning_recovery  = 560000000
    critical          = 600000000
    critical_recovery = 590000000
  }

  notify_no_data    = false
  renotify_interval = 90

  notify_audit = false
  timeout_h    = 60
  include_tags = true

  silenced = {
    "*" = 0
  }

}
