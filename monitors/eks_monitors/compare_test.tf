resource "datadog_monitor" "compare_test" {
  name = "Composite Monitor"
  type = "composite"
  message = "This is a test to compare "

	query = "(${datadog_monitor.test_networkin.id}  &&  ${datadog_monitor.test_networkout.id})"
}
