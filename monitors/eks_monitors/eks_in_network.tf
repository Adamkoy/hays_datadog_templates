
resource "datadog_monitor" "test_networkin" {
  name               = " test EKS network in A"
  type               = "metric alert"
  message            = "Monitor triggered. Notify: @slack"
  escalation_message = "Escalation message @a.koyuncu@cyc.ltd"

  query = "avg(last_1h):avg:kubernetes.network.rx_bytes{*} by {pod_name,instance-type, kube_namespace} > 300000"

  thresholds = {
    ok                = 220000
    warning           = 229000
    warning_recovery  = 220000
    critical          = 300000
    critical_recovery = 250000
  }

  notify_no_data    = false
  renotify_interval = 90

  notify_audit = false
  timeout_h    = 60
  include_tags = true



}
