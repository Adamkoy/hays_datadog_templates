#Total CPU in use on a kubernetes

resource "datadog_monitor" "cpumonitor" {
  name           = "Ireland_DevOps_EKS_CPU_Usage"
  type           = "metric alert"
  message        = "Total CPU usage of kubernetes: cpu usage of this {{pod_name.name}} has reach is using excess CPU power Notify:8d7afe15.ACNLTD.onmicrosoft.com@uk.teams.ms"

  notify_no_data = true
  no_data_timeframe = 25     # The number of minutes before a monitor will notify when data stops reporting
  include_tags = true
  new_host_delay= 300

  notify_audit = false    # whether tagged users will be notified on changes to this monitor
  timeout_h    = 60       # The number of hours of the monitor not reporting data before it will automatically resolve from a triggered state


  # This scope will be muted until the given timestamp or forever if the value is 0. Use -1 if you want to unmute the scope.
  # To mute the alert completely
  silenced = {
    "*" = 0
  }

  query = "avg(last_1d):avg:kubernetes.cpu.usage.total{*} by {pod_name,pod_phase,environment,instance-type,host} > 195000000"


  thresholds = {
     warning           = 180000000        # 180 mcores
     warning_recovery  = 175000000        # 175 mcores
     critical          = 195000000        # 195 mcores
     critical_recovery = 190000000
   }

}
