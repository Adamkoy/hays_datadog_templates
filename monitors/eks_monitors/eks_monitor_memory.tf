# Total memory in use on a node or pod
resource "datadog_monitor" "memory_usage" {
  name           = "Ireland_DevOps_EKS_Memory_Usage"
  type           = "metric alert"
  message        = "kububents memory usage is acting unusual {{pod_name.name}}, {{host.name}}, {{environment.name}}   Notify:8d7afe15.ACNLTD.onmicrosoft.com@uk.teams.ms"

  notify_no_data = true
  renotify_interval = 60
  no_data_timeframe = 25   # The number of minutes before a monitor will notify when data stops reporting
  include_tags = true
  new_host_delay= 300


  notify_audit = false    # whether tagged users will be notified on changes to this monitor
  timeout_h    = 60       # The number of hours of the monitor not reporting data before it will automatically resolve from a triggered state


  # This scope will be muted until the given timestamp or forever if the value is 0. Use -1 if you want to unmute the scope.
  # To mute the alert completely
  silenced = {
    "*" = 0
  }


  query = "avg(last_1h):avg:kubernetes.memory.usage{*} by {host,instance-type,environment,pod_phase,pod_name} > 2500000000"

  thresholds = {
     ok                = 1830000000
     warning           =1870000000      #(1.74 GiB)
     warning_recovery  =1800000000      #(1.68 GiB)
     critical          = 2500000000     # (2.33 GiB)
     critical_recovery = 2200000000     #(2.05 GiB)
   }
}
