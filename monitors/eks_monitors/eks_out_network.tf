
resource "datadog_monitor" "test_networkout" {
  name               = "Test EKS network out pods A"
  type               = "metric alert"
  message            = "Monitor triggered. Notify: @slack"
  escalation_message = "Escalation message @a.koyuncu@cyc.ltd"

  query = "avg(last_1h):avg:kubernetes.network.tx_bytes{*} by {pod_name,instance-type, kube_namespace} > 35000"

  thresholds = {
    ok                = 28000
    warning           = 29000
    warning_recovery  = 27000
    critical          = 35000
    critical_recovery = 30000
  }

  notify_no_data    = false
  renotify_interval = 90

  notify_audit = true
  timeout_h    = 60
  include_tags = true



}
